import torch
import torch.nn as nn
from torch.nn.utils.rnn import pad_packed_sequence

import config
from torch.autograd import Variable
import torch.nn.functional as F
import copy
from transformers import BertForSequenceClassification,BertConfig

def clones(module,N):
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])

class LSTM_Update(nn.Module):
    def __init__(self,in_dim,hidden_dim,dropout):
        super(LSTM_Update,self).__init__()
        self.in_dim=in_dim
        self.hidden_dim=hidden_dim
        self.dropout=nn.Dropout(dropout)
        
        self.in_proj=clones(nn.Linear(in_dim,hidden_dim),4)
        self.hidden_proj=clones(nn.Linear(hidden_dim,hidden_dim),4)
        self.context_proj=clones(nn.Linear(hidden_dim,hidden_dim),3)
        
    def forward(self,word,hidden,context,c_hate):
        total=[] #stores i_t,f_t,o_t
        for i in range(3):
            result=torch.sigmoid(self.in_proj[i](word)+self.hidden_proj[i](hidden)+self.context_proj[i](context))
            total.append(result)
        c_t=total[1] * context + total[0] * c_hate
        h_t=total[2] * torch.tanh(c_t)
        return h_t,c_t
    
class Bi_LSTM_Update(nn.Module):
    def __init__(self,in_dim,hidden_dim):
        super(Bi_LSTM_Update,self).__init__()
        self.in_dim=in_dim
        self.hidden_dim=hidden_dim
        
        self.in_proj=clones(nn.Linear(in_dim,hidden_dim),4)
        self.hidden_proj=clones(nn.Linear(hidden_dim,hidden_dim),4)
        self.context_proj=clones(nn.Linear(hidden_dim,hidden_dim),3)
        
    def forward(self,word,hidden,context):
        total=[] #stores i_t,f_t,o_t
        for i in range(3):
            result=torch.sigmoid(self.in_proj[i](word)+self.hidden_proj[i](hidden)+self.context_proj[i](context))
            total.append(result)
        c_hate=torch.tanh(self.in_proj[3](word) + self.hidden_proj[3](hidden))
        c_t=total[1] * context + total[0] * c_hate
        h_t=total[2] * torch.tanh(c_t)
        return h_t,c_t
    
class Bi_LSTM(nn.Module):
    def __init__(self,in_dim,hidden_dim,dropout):
        super(Bi_LSTM,self).__init__()
        self.lstms=clones(Bi_LSTM_Update(in_dim,hidden_dim),2)
        self.hidden_dim=hidden_dim
        
    def forward(self,seq):
        h0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda()),Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())]
        c0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda()),Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())]
        hidden=[[h0[0].unsqueeze(1)],[h0[1].unsqueeze(1)]]
        context=[[c0[0]],[c0[1]]]
        for idx in range(seq.size()[1]):
            word=[seq[:,idx,:],seq[:,-(idx+1),:]]
            for i, w in enumerate(word):
                h=hidden[i][0].squeeze()
                c=context[i][0]
                new_h,new_c=self.lstms[i](word[i],h,c)
                if i==0:
                    hidden[i].append(new_h.unsqueeze(1))
                    context[i].append(new_c)
                else:
                    hidden[i].insert(0,new_h.unsqueeze(1))
                    context[i].insert(0,new_c)
        forward_h=torch.cat(hidden[0],dim=1)
        backward_h=torch.cat(hidden[1],dim=1)
        final_h=torch.cat((forward_h,backward_h),dim=2)
        return final_h

'''class Coupled_Layer(nn.Module):
    def __init__(self,in_dim,hidden_dim,dropout=0.3):
        super(Coupled_Layer,self).__init__()
        """
        two LSTMs sharing information with each other
        """
        self.lstm=clones(LSTM_Update(in_dim,hidden_dim,dropout),2)
        
        self.proj_word=clones(nn.Linear(in_dim,hidden_dim),2)
        
        self.gate_proj_w=clones(nn.Linear(in_dim,1),2)#W_g
        self.gate_proj_u=clones(nn.Linear(hidden_dim,1),2)#U_g
        
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),2)
        
        self.hidden_dim=hidden_dim
        
    def forward(self,seq,num_task=0):
        h0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda()),Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())]
        c0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda()),Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())]
        
        other=1-num_task
        for idx in range(seq.size()[1]):
            word=seq[:,idx,:]
            proj_word=self.proj_word[num_task](word)
            g_own=torch.sigmoid(self.gate_proj_w[num_task](word) + self.gate_proj_u[num_task](h0[num_task]))
            g_other=torch.sigmoid(self.gate_proj_w[num_task](word) + self.gate_proj_u[other](h0[other]))
            c_hate=torch.tanh(proj_word + g_own*self.proj_hidden[num_task](h0[num_task]) + g_other*self.proj_hidden[other](h0[other]))
            h,c=self.lstm[num_task](word,h0[num_task],c0[num_task],c_hate)
            #print(h.shape,c.shape)
            h0[num_task]=h
            c0[num_task]=c
            
            """
           updating the hidden states and context in the other LSTM 
            """
            proj_word=self.proj_word[other](word)
            c_hate=torch.tanh(proj_word + g_own + g_other)
            h,c=self.lstm[other](word,h0[other],c0[other],c_hate)
            h0[other]=h
            c0[other]=c
            
        return h0[num_task],c0[num_task]'''

#extension for the coupling layer
#previously for binary tasks now extend to multiple tasks
class Coupled_Layer(nn.Module):
    def __init__(self,in_dim,hidden_dim,num_task,dropout=0.3):
        super(Coupled_Layer,self).__init__()
        """
        two LSTMs sharing information with each other
        """
        self.lstm=clones(LSTM_Update(in_dim,hidden_dim,dropout),num_task)
        
        self.proj_word=clones(nn.Linear(in_dim,hidden_dim),num_task)#W_c
        
        self.gate_proj_w=clones(nn.Linear(in_dim,1),num_task)#W_gc
        self.gate_proj_u=clones(nn.Linear(hidden_dim,1),num_task)#U_gc
        
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),num_task*num_task)#U_c
        
        self.hidden_dim=hidden_dim
        self.tasks=num_task
        
    def forward(self,seq,num_task=0):
        h0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        c0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        
        h_latest=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        c_latest=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        for idx in range(seq.size()[1]):
            word=seq[:,idx,:]
            #proj_word=self.proj_word[num_task](word)
            #print(h.shape,c.shape)  
            for k in range(self.tasks):
                total=self.proj_word[k](word)
                for j in range(self.tasks):
                    g_own=torch.sigmoid(self.gate_proj_w[k](word) + self.gate_proj_u[j](h0[j]))#g_jk
                    cur_other=g_own * self.proj_hidden[k*self.tasks+j](h0[j])
                    total=total+cur_other
                #update hidden states in other lstms
                c_hate=torch.tanh(total)
                h,c=self.lstm[num_task](word,h0[k],c0[k],c_hate) 
                h_latest[k]=h
                c_latest[k]=c 
            h0=h_latest
            c0=c_latest
        return h0[num_task],c0[num_task]

class Local_Layer(nn.Module):
    def __init__(self,in_dim,hidden_dim,num_task,dropout=0.3):
        super(Coupled_Layer,self).__init__()
        """
        two LSTMs sharing information with each other
        """
        self.lstm=clones(LSTM_Update(in_dim,hidden_dim,dropout),num_task)
        
        self.proj_first=clones(nn.Linear(in_dim,hidden_dim),num_task)
        self.proj_word=clones(nn.Linear(in_dim,hidden_dim),num_task)#W_c
        
        self.gate_proj_w=clones(nn.Linear(in_dim,1),num_task)#W_gc
        self.gate_proj_u=clones(nn.Linear(hidden_dim,1),num_task)#U_gc
        
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),num_task*num_task)#U_cc
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),num_task*num_task)#U_c
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),num_task)#U_gf
        self.hidden_dim=hidden_dim
        self.tasks=num_task
        
    def forward(self,seq,num_task=0):
        h0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        c0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        
        h_latest=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        c_latest=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        for idx in range(seq.size()[1]):
            word=seq[:,idx,:]
            #proj_word=self.proj_word[num_task](word)
            #print(h.shape,c.shape)  
            for k in range(self.tasks):
                total=self.proj_word[k](word)
                for j in range(self.tasks):
                    g_own=torch.sigmoid(self.gate_proj_w[k](word) + self.gate_proj_u[j](h0[j]))#g_jk
                    cur_other=g_own * self.proj_hidden[k*self.tasks+j](h0[j])
                    total=total+cur_other
                #update hidden states in other lstms
                c_t=torch.tanh(total)
                proj_x=self.proj_first[k](word)
                #computation of LF
                c_hate=torch.tanh(proj_x+c_t+LF)
                h,c=self.lstm[num_task](word,h0[k],c0[k],c_hate) 
                h_latest[k]=h
                c_latest[k]=c 
            h0=h_latest
            c0=c_latest
        return h0[num_task],c0[num_task]    
    
class Shared_Layer(nn.Module):
    def __init__(self,in_dim,hidden_dim,num_task,gate,dropout=0.3):
        super(Shared_Layer,self).__init__()
        """
        two LSTMs sharing information with each other
        """
        self.tasks=num_task
        self.gate=gate
        
        self.lstm=clones(LSTM_Update(in_dim,hidden_dim,dropout),num_task)
        self.shared_lstm=Bi_LSTM(in_dim,hidden_dim,dropout)
        # self.shared_bert=BertForSequenceClassification.from_pretrained(
        #     'bert-base-uncased',
        #     num_labels=num_task,
        #     output_attentions=False,
        #     output_hidden_states=True
        # )
        self.proj_word=clones(nn.Linear(in_dim,hidden_dim),num_task)
        self.gate_proj_w=clones(nn.Linear(in_dim,1),num_task)#W_g
        self.gate_proj_u=clones(nn.Linear(hidden_dim,1),num_task)#U_g
        self.proj_hidden=clones(nn.Linear(hidden_dim,hidden_dim),num_task)
        
        self.shared_hidden_proj=nn.Linear(2*hidden_dim,hidden_dim)
        self.shared_gate_w=clones(nn.Linear(in_dim,1),num_task)#W_g
        self.shared_gate_u=clones(nn.Linear(2*hidden_dim,1),num_task)#U_g
        
        self.bert_proj=nn.Linear(768,2*hidden_dim)
        self.bert_proj1=nn.Linear(768,hidden_dim)
        self.hidden_dim=hidden_dim
        
    def forward(self,seq,num_task=0):
        h0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        c0=[Variable(torch.zeros(seq.size()[0],self.hidden_dim).cuda())] * self.tasks
        
        shared_hidden=self.shared_lstm(seq)
        '''shared=self.shared_lstm(bert_tokens,token_type_ids=None,attention_mask=masks)
        shared_hidden=self.bert_proj(shared[1][-1])
        shared=self.shared_bert(bert_tokens,token_type_ids=None,attention_mask=masks)
        global_bert=self.bert_proj1(shared[1][-1][:,0,:])'''
        #print (global_bert.shape)
        for idx in range(seq.size()[1]):
            word=seq[:,idx,:]
            hidden=shared_hidden[:,idx,:]
            proj_word=self.proj_word[num_task](word)
            g_own=torch.sigmoid(self.gate_proj_w[num_task](word) + self.gate_proj_u[num_task](h0[num_task]))
            g_share=torch.sigmoid(self.shared_gate_w[num_task](word) + self.shared_gate_u[num_task](hidden))
            c_hate=torch.tanh(proj_word + g_own*self.proj_hidden[num_task](h0[num_task]) + g_share*self.shared_hidden_proj(hidden))
            h,c=self.lstm[num_task](word,h0[num_task],c0[num_task],c_hate) 
            #print(h.shape,c.shape)
            h0[num_task]=h
            c0[num_task]=c  
        #global_fusion=self.gate(h0[num_task],global_bert)
        global_fusion=h0[num_task]
        #print (global_fusion.shape)
        return global_fusion,c0[num_task]

    
class Full_RNN(nn.Module):
    def __init__(self,in_dim,num_hidden,num_layer,bidirect,dropout,rnn_type='LSTM'):
        super(Full_RNN,self).__init__()
        rnn_cls=nn.LSTM if rnn_type=='LSTM' else nn.GRU
        self.rnn=rnn_cls(in_dim,num_hidden,num_layer,bidirectional=bidirect,dropout=dropout,batch_first=True)
        self.in_dim=in_dim
        self.num_hidden=num_hidden
        self.num_layer=num_layer
        self.rnn_type=rnn_type
        self.num_bidirect=1+int(bidirect)
        self.bidirect=bidirect
        
    def init_hidden(self,batch):
        weight=next(self.parameters()).data
        hid_shape=(self.num_layer * self.num_bidirect,batch,self.num_hidden)
        if self.rnn_type =='LSTM':
            return (Variable(weight.new(*hid_shape).zero_().cuda()),
                    Variable(weight.new(*hid_shape).zero_().cuda()))
        else:
            return Variable(weight.new(*hid_shape).zero_()).cuda()
    
    def forward(self,x):
        batch=x.size(0)
        hidden=self.init_hidden(batch)
        self.rnn.flatten_parameters()
        output,hidden=self.rnn(x,hidden)
        if self.bidirect:
            hidden = torch.cat((hidden[0][0], hidden[0][1]), dim=1)
            hidden = hidden.squeeze()
        else:
            hidden = hidden[0].squeeze()
        return output,hidden

class BiLSTMPacked(nn.Module):
    def __init__(self, in_dim, num_hidden, num_layer, bidirect, dropout, rnn_type='LSTM'):
        super(BiLSTMPacked, self).__init__()
        rnn_cls = nn.LSTM if rnn_type == 'LSTM' else nn.GRU
        self.rnn = rnn_cls(in_dim, num_hidden, num_layer, bidirectional=bidirect, dropout=dropout, batch_first=True)
        self.num_layer=num_layer
        self.num_hidden=num_hidden
        self.rnn_type=rnn_type
        self.num_bidirect=1+int(bidirect)
        self.bidirect=bidirect

    def init_hidden(self,batch):
        weight=next(self.parameters()).data
        hid_shape=(self.num_layer * self.num_bidirect,batch,self.num_hidden)
        if self.rnn_type =='LSTM':
            return (Variable(weight.new(*hid_shape).zero_().cuda()),
                    Variable(weight.new(*hid_shape).zero_().cuda()))
        else:
            return Variable(weight.new(*hid_shape).zero_()).cuda()

    def forward(self, x):
        hidden = self.init_hidden(64)
        self.rnn.flatten_parameters()
        pack_output, hidden = self.rnn(x)
        #print(pack_output.shape, hidden.shape)
        output, _ = pad_packed_sequence(pack_output, batch_first=True)
        batch = output.size(0)
        if self.bidirect:
            hidden = hidden[0].view(self.num_layer,self.num_bidirect,batch,self.num_hidden)[-1]
            hidden = torch.cat((hidden[0], hidden[1]), dim=1)
        else:
            hidden = hidden[0].view(self.num_layer,self.num_bidirect,batch,self.num_hidden)[-1]
            hidden = hidden.squeeze()
        return output, hidden

class CNN_Model(nn.Module):
    def __init__(self,in_dim,filter_size,num_filter):
        super(CNN_Model,self).__init__()
        self.in_dim=in_dim
        filter_sizes=[int(fsz) for fsz in filter_size.split(',')]
        self.conv=nn.ModuleList([nn.Conv2d(1,num_filter,(fsz,in_dim)) for fsz in filter_sizes])
        self.pool=nn.MaxPool1d(kernel_size=4, stride=4)
        
    def forward(self,emb):
        emb=emb.unsqueeze(1)#B,1,L,D
        conv_result=[F.relu(conv(emb)) for conv in self.conv]
        pool_result=[F.max_pool2d(input=x_i,kernel_size=(x_i.shape[2],x_i.shape[3])) for x_i in conv_result]
        mid=[torch.squeeze(x_i) for x_i in pool_result]
        final=torch.cat(mid,1)
        return final
    
class Part_RNN(nn.Module):
    def __init__(self,in_dim,num_hidden,num_layer,bidirect,dropout,rnn_type='LSTM'):
        super(Part_RNN,self).__init__()
        rnn_cls=nn.LSTM if rnn_type=='LSTM' else nn.GRU
        self.rnn=rnn_cls(in_dim,num_hidden,num_layer,bidirectional=bidirect,dropout=dropout,batch_first=True)
        self.in_dim=in_dim
        self.num_hidden=num_hidden
        self.num_layer=num_layer
        self.rnn_type=rnn_type
        self.num_bidirect=1+int(bidirect)
        
    def init_hidden(self,batch):
        weight=next(self.parameters()).data
        hid_shape=(self.num_layer * self.num_bidirect,batch,self.num_hidden)
        if self.rnn_type =='LSTM':
            return (Variable(weight.new(*hid_shape).zero_().cuda()),
                    Variable(weight.new(*hid_shape).zero_().cuda()))
        else:
            return Variable(weight.new(*hid_shape).zero_()).cuda()
    
    def forward(self,x):
        batch=x.size(0)
        hidden=self.init_hidden(batch)
        self.rnn.flatten_parameters()
        output,hidden=self.rnn(x,hidden)
        return output[:,-1,:]
