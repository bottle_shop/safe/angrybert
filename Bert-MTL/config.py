import argparse 

def parse_opt():
    parser=argparse.ArgumentParser()
    
    """
    basic: shared and private rnns, concatenation of both to the fc layer
    dnn: shared and private embeddings, private rnn (mt-dnn similar)
    cnn: shared and private embeddings, private cnn
    uniform: different embeddings, shared rnn, 2016 IJCAI first baseline
    local: 2016 IJCAI, shared layer in the paper
    sp-mtl: 2016 IJCAI, for the implementation of global fusion
    mtl-gatedencoder: joint modeling network, Rajamanickam et al. 20
    shared-bert: bert baseline model for multitask shared learning
    angrybert: AngryBERT model, this is our proposed model
    angrybert-attn: AngryBERT model with attention on top, visualization purpose
    """
    
    parser.add_argument('--MODEL',type=str,default='angrybert')
    
    '''path configuration'''
    parser.add_argument('--GLOVE_PATH',type=str,default='/data/data_store/rabiul/pre-embeddings/glove.6B.300d.txt')
    #path for pre-precessing and result saving
    parser.add_argument('--DT',type=str,default='./dt')
    parser.add_argument('--WZ_RESULT',type=str,default='./wz')
    parser.add_argument('--FOUNTA_RESULT',type=str,default='./founta')
    parser.add_argument('--HATELINGO_RESULT',type=str,default='./hatelingo')
    parser.add_argument('--DICT_INFO',type=str,default='./dictionary')
    
    #path for the split dataset
    parser.add_argument('--SPLIT_DATASET',type=str,default='/student/mda219/workspace/angrybert/resource')
    
    
    '''hyper parameters configuration'''
    parser.add_argument('--EMB_DROPOUT',type=float,default=0.5)
    parser.add_argument('--FC_DROPOUT',type=float,default=0.2) 
    parser.add_argument('--MIN_OCC',type=int,default=3)
    parser.add_argument('--BATCH_SIZE',type=int,default=60)
    parser.add_argument('--EMB_DIM',type=int,default=300)
    parser.add_argument('--MID_DIM',type=int,default=256)
    parser.add_argument('--PROJ_DIM',type=int,default=32)
    parser.add_argument('--NUM_HIDDEN',type=int,default=128)
    parser.add_argument('--NUM_FILTER',type=int,default=150)
    parser.add_argument('--FILTER_SIZE',type=str,default="2,3,4")
    parser.add_argument('--NUM_LAYER',type=int,default=1)
    parser.add_argument('--BIDIRECT',type=bool,default=True)
    parser.add_argument('--L_RNN_DROPOUT',type=float,default=0.1) 
    """
    wz, dt, founta, hatelingo, offenseval_c, semeval_a
    it is a str and the first position of the str is the hate speech detection dataset
    """
    parser.add_argument('--TASKS',type=str,default='wz,hatelingo')
    parser.add_argument('--LENGTH',type=int,default=30)
    
    parser.add_argument('--CREATE_DICT',type=bool,default=True)
    parser.add_argument('--CREATE_EMB',type=bool,default=True)
    parser.add_argument('--SAVE_NUM',type=int,default=1)
    parser.add_argument('--EPOCHS',type=int,default=6)
    parser.add_argument('--CROSS_VAL',type=int,default=5)
    
    parser.add_argument('--SEED', type=int, default=1112, help='random seed')
    parser.add_argument('--CUDA_DEVICE', type=int, default=0)
    
    
    parser.add_argument('--EVAL_ITERS', type=int, default=180)
    parser.add_argument('--TOTAL_ITERS', type=int, default=1800)
    
    args=parser.parse_args()
    return args
