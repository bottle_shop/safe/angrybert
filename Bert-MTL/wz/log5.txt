MODEL : fusion
GLOVE_PATH : /home/ruicao/trained/embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : ../angry-MTL/split_data_tokens
EMB_DROPOUT : 0.5
FC_DROPOUT : 0.2
MIN_OCC : 5
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 128
PROJ_DIM : 32
NUM_HIDDEN : 200
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : False
L_RNN_DROPOUT : 0.3
TASKS : wz,harassment
LENGTH : 30
CREATE_DICT : True
CREATE_EMB : True
SAVE_NUM : 5
EPOCHS : 6
CROSS_VAL : 10
SEED : 1111
CUDA_DEVICE : 7
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 1.37
	eval task wz precision: 51.02 
	eval task wz recall: 63.46 
	eval task wz f1: 55.91 

	eval task harassment precision: 59.91 
	eval task harassment recall: 45.12 
	eval task harassment f1: 48.03 

epoch 1
	 multi task train_loss: 202.89
	eval task wz precision: 79.52 
	eval task wz recall: 77.70 
	eval task wz f1: 77.73 

	eval task harassment precision: 53.64 
	eval task harassment recall: 73.24 
	eval task harassment f1: 61.93 

epoch 2
	 multi task train_loss: 176.88
	eval task wz precision: 81.71 
	eval task wz recall: 78.25 
	eval task wz f1: 78.97 

	eval task harassment precision: 61.96 
	eval task harassment recall: 73.14 
	eval task harassment f1: 63.47 

epoch 3
	 multi task train_loss: 167.66
	eval task wz precision: 80.90 
	eval task wz recall: 73.80 
	eval task wz f1: 75.43 

	eval task harassment precision: 57.28 
	eval task harassment recall: 67.58 
	eval task harassment f1: 61.18 

epoch 4
	 multi task train_loss: 160.88
	eval task wz precision: 83.92 
	eval task wz recall: 81.37 
	eval task wz f1: 82.01 

	eval task harassment precision: 58.88 
	eval task harassment recall: 68.16 
	eval task harassment f1: 62.02 

epoch 5
	 multi task train_loss: 157.84
	eval task wz precision: 84.78 
	eval task wz recall: 81.01 
	eval task wz f1: 81.84 

	eval task harassment precision: 57.98 
	eval task harassment recall: 68.70 
	eval task harassment f1: 61.64 

epoch 6
	 multi task train_loss: 153.37
	eval task wz precision: 83.60 
	eval task wz recall: 82.57 
	eval task wz f1: 82.02 

	eval task harassment precision: 59.38 
	eval task harassment recall: 71.53 
	eval task harassment f1: 62.87 

epoch 7
	 multi task train_loss: 148.45
	eval task wz precision: 84.34 
	eval task wz recall: 80.23 
	eval task wz f1: 81.05 

	eval task harassment precision: 59.17 
	eval task harassment recall: 66.80 
	eval task harassment f1: 61.98 

epoch 8
	 multi task train_loss: 146.17
	eval task wz precision: 83.54 
	eval task wz recall: 81.67 
	eval task wz f1: 81.34 

	eval task harassment precision: 59.90 
	eval task harassment recall: 69.63 
	eval task harassment f1: 62.98 

epoch 9
	 multi task train_loss: 141.26
	eval task wz precision: 80.82 
	eval task wz recall: 78.79 
	eval task wz f1: 78.77 

	eval task harassment precision: 62.46 
	eval task harassment recall: 70.17 
	eval task harassment f1: 64.37 

