HATELINGO_RESULT : ./hatelingo
WZ_RESULT : ./wz
PROJ_DIM : 32
GLOVE_PATH : /home/caorui/disk_data/videoQA/glove.6B.300d.txt
DT : ./dt
SEED : 1111
EVAL_ITERS : 180
TASK2 : hatelingo
CROSS_VAL : 5
SPLIT_DATASET : ./split_data
NUM_LAYER : 1
EPOCHS : 10
LENGTH : 30
CREATE_DICT : False
TASK1 : wz
MODEL : gate
CREATE_EMB : False
MIN_OCC : 3
MID_DIM : 128
BATCH_SIZE : 128
L_RNN_DROPOUT : 0.3
FOUNTA_RESULT : ./founta
DICT_INFO : ./dictionary
NUM_HIDDEN : 200
FC_DROPOUT : 0.2
EMB_DROPOUT : 0.5
TOTAL_ITERS : 1620
BIDIRECT : False
SAVE_NUM : 6
CUDA_DEVICE : 0
EMB_DIM : 300
epoch 0
	train_loss: 1.39, hate loss: 0.69, lingo loss: 0.69
	eval t1 f1: 9.65 
	eval t1 precision: 41.02 
	eval t1 recall: 5.47 
	eval t2 f1: 4.09 
	eval t2 precision: 7.22 
	eval t2 recall: 7.81 
epoch 1
	train_loss: 67.72, hate loss: 47.21, lingo loss: 20.51
	eval t1 f1: 75.74 
	eval t1 precision: 81.82 
	eval t1 recall: 80.47 
	eval t2 f1: 83.58 
	eval t2 precision: 83.04 
	eval t2 recall: 84.38 
epoch 2
	train_loss: 39.08, hate loss: 33.55, lingo loss: 5.54
	eval t1 f1: 67.73 
	eval t1 precision: 75.59 
	eval t1 recall: 73.44 
	eval t2 f1: 86.96 
	eval t2 precision: 89.35 
	eval t2 recall: 87.50 
epoch 3
	train_loss: 33.93, hate loss: 29.04, lingo loss: 4.89
	eval t1 f1: 80.72 
	eval t1 precision: 79.82 
	eval t1 recall: 82.03 
	eval t2 f1: 91.49 
	eval t2 precision: 93.12 
	eval t2 recall: 91.41 
epoch 4
	train_loss: 28.82, hate loss: 24.57, lingo loss: 4.25
	eval t1 f1: 75.12 
	eval t1 precision: 74.30 
	eval t1 recall: 76.56 
	eval t2 f1: 92.42 
	eval t2 precision: 91.49 
	eval t2 recall: 93.75 
epoch 5
	train_loss: 25.31, hate loss: 21.42, lingo loss: 3.89
	eval t1 f1: 68.03 
	eval t1 precision: 67.41 
	eval t1 recall: 70.31 
	eval t2 f1: 85.49 
	eval t2 precision: 85.79 
	eval t2 recall: 86.72 
epoch 6
	train_loss: 21.55, hate loss: 18.00, lingo loss: 3.54
	eval t1 f1: 78.71 
	eval t1 precision: 79.51 
	eval t1 recall: 78.12 
	eval t2 f1: 88.19 
	eval t2 precision: 88.89 
	eval t2 recall: 89.06 
epoch 7
	train_loss: 18.05, hate loss: 14.77, lingo loss: 3.28
	eval t1 f1: 71.57 
	eval t1 precision: 73.23 
	eval t1 recall: 74.22 
	eval t2 f1: 88.83 
	eval t2 precision: 90.71 
	eval t2 recall: 89.06 
epoch 8
	train_loss: 15.66, hate loss: 12.52, lingo loss: 3.14
	eval t1 f1: 75.24 
	eval t1 precision: 76.82 
	eval t1 recall: 74.22 
	eval t2 f1: 93.42 
	eval t2 precision: 94.59 
	eval t2 recall: 92.97 
