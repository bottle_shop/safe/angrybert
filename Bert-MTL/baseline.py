import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence
from torch.autograd import Function
import torch.nn.functional as F


import config

from full_rnn import Full_RNN, Coupled_Layer, Shared_Layer, CNN_Model, Local_Layer, BiLSTMPacked
from language_model import Word_Embedding
from classifier import SimpleClassifier
from train import bce_for_loss
from attention import Gate_Attention, SelfAttention
import copy
from transformers import BertForSequenceClassification,BertConfig

def clones(module,N):
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])

"""
all previous implementations have been removed 
(since our previous operation is for two inputs in the forward function)
if wanna to check 
please refer to previous codes
"""



class Joint_Multi(nn.Module):
    def __init__(self,shared_emb,private_emb,shared_rnn,private_rnn,gates,fcs,mode,opt,atts=None,rnn_last=None):
        super(Joint_Multi,self).__init__()
        self.w_emb=shared_emb
        self.embs=private_emb
        self.s_rnn=shared_rnn
        self.p_rnn=private_rnn
        self.gates=gates
        self.fcs=fcs
        self.atts = atts
        self.mode=mode
        self.rnn_last = rnn_last
        self.drop = nn.Dropout(0.20)
    def forward(self,text,task_idx=0,bert_tokens=None,masks=None,att_masks=None): 
        shared_emb=self.w_emb(text)
        private_emb=self.embs(text)
        
        lens = (att_masks != 0).sum(dim=1).cuda()

        packed_input = pack_padded_sequence(private_emb, lengths=lens, batch_first=True)
        s_packed_input = pack_padded_sequence(shared_emb, lengths=lens, batch_first=True)
        s_out, shared_rnn=self.s_rnn(s_packed_input)

        if task_idx == 0:
            p_out, private_rnn=self.p_rnn(packed_input)  
            p_out = self.drop(p_out)
            gated_private_rnn = p_out*0.1 + s_out*0.9
            gated_private_rnn = pack_padded_sequence(gated_private_rnn, lengths=lens, batch_first=True)
            p_out, rnn_o = self.rnn_last(gated_private_rnn)
        else: 
            s_out = self.drop(s_out)
            s_out = pack_padded_sequence(s_out, lengths=lens, batch_first=True)
            p_out, rnn_o = self.rnn_last(s_out)
        result, _ = self.atts[task_idx](rnn_o, p_out, p_out, att_masks)

        logits=self.fcs[task_idx](result)
        return logits, _


class Deep_Multi(nn.Module):
    def __init__(self,shared_emb,private_emb,shared_rnn,private_rnn,gates,fcs,mode,opt,atts=None):
        super(Deep_Multi,self).__init__()
        self.w_emb=shared_emb
        self.embs=private_emb
        self.s_rnn=shared_rnn
        self.rnns=private_rnn
        self.gates=gates
        self.fcs=fcs
        self.atts = atts
        self.proj=nn.Linear(768,opt.NUM_HIDDEN*2)
        self.mode=mode 
        
    def forward(self,text,task_idx=0,bert_tokens=None,masks=None,att_masks=None):
        weights = None
        shared_emb=self.w_emb(text)
        private_emb=self.embs[task_idx](text)
        if self.mode in ['gate','dnn','cnn','uniform']:
            embedding=torch.cat((shared_emb,private_emb),dim=2)
        elif self.mode in ['basic','angrybert','angrybert-attn']:
            embedding = private_emb
        
        if self.mode=='cnn':
            shared_rnn=self.s_rnn(embedding)
            private_rnn=self.rnns[task_idx](embedding)

        elif self.mode == 'shared-bert':
            shared_rnn = self.s_rnn(bert_tokens, token_type_ids=None, attention_mask=masks)
            shared_rnn = shared_rnn[1][-1][:, 0, :]

            result = self.proj(shared_rnn)

        elif self.mode=='angrybert':
            shared_rnn=self.s_rnn(bert_tokens,token_type_ids=None,attention_mask=masks)
            shared_rnn=shared_rnn[1][-1][:,0,:]
            
            shared_rnn=self.proj(shared_rnn)
            shared_rnn=F.relu(shared_rnn)
            lens = (att_masks != 0).sum(dim=1).cuda()
            packed_input = pack_padded_sequence(embedding, lengths=lens, batch_first=True)
            _,private_rnn=self.rnns[task_idx](packed_input)

        elif self.mode=='angrybert-attn':
            shared_rnn=self.s_rnn(bert_tokens,token_type_ids=None,attention_mask=masks)
            shared_rnn=shared_rnn[1][-1][:,0,:]

            shared_rnn=self.proj(shared_rnn)
            lens = (att_masks != 0).sum(dim=1).cuda()
            packed_input = pack_padded_sequence(embedding, lengths=lens, batch_first=True)
            p_out,private_rnn=self.rnns[task_idx](packed_input)
            p_attn, weights = self.atts[task_idx](private_rnn, p_out, p_out, att_masks)
            result = self.gates[task_idx](shared_rnn, p_attn)
        else:
            lens = (att_masks != 0).sum(dim=1).cuda()
            packed_input = pack_padded_sequence(embedding, lengths=lens, batch_first=True)
            _,shared_rnn=self.s_rnn(packed_input)
            _,private_rnn=self.rnns[task_idx](packed_input)
        

        if self.mode in ['gate','angrybert']:
            result=self.gates[task_idx](shared_rnn,private_rnn)
        elif self.mode=='basic':
            result=torch.cat((shared_rnn,private_rnn),dim=1)
        elif self.mode in ['dnn','cnn']:
            result=private_rnn
        elif self.mode=='uniform':
            result=shared_rnn
        logits=self.fcs[task_idx](result)
        return logits, weights
    
class Deep_Coupled(nn.Module):
    def __init__(self,shared_emb,private_emb,couple,fcs):
        super(Deep_Coupled,self).__init__()
        self.embs=private_emb
        self.w_emb=shared_emb #not used
        self.couple=couple
        self.fcs=fcs

    def forward(self,text,task_idx=0,bert_tokens=None,masks=None,att_masks=None):
        w_emb=self.embs[task_idx](text)
        result,_=self.couple(w_emb,task_idx)
        logits=self.fcs[task_idx](result)

        return logits, _
    
def build_baseline(dataset,opt):
    """
    what models do we provide in the baseline part:
    basic: shared and private rnns, concatenation of both to the fc layer
    dnn: shared and private embeddings, private rnn
    cnn: shared and private embeddings, private cnn
    uniform: different embeddings, shared rnnm, 2016 IJCAI first baseline
    local: 2016 IJCAI, shared layer in the paper
    sp-mtl: 2017 IJCAI, for the implementation of global fusion
    mtl-gatedencoder: joint modeling network, Rajamanickam et al. 20
    shared-bert: bert baseline model for multitask shared learning
    angrybert: AngryBERT model, this is our proposed model
    angrybert-attention: AngryBERT model with attention on top, visualization purpose
    """
    opt= config.parse_opt()
    mode=opt.MODEL
    em_times=1
    fc_times=1
    fin_times = 1

    if mode in ['gate','dnn','cnn','uniform']:
        em_times=2
    if mode in ['basic', 'angrybert','angrybert-attn', 'shared-bert']:
        fc_times=2

    if mode in ["dnn", "uniform", "angrybert", "angrybert-attn", 'shared-bert', "mtl-gatedencoder"]:
        fin_times = 2
    elif mode  == "basic":
        fin_times = 4

    datasets=opt.TASKS.split(',')
    num_tasks=len(datasets)
    fcs=nn.ModuleList()
    task2dim={'wz':3,'dt':3,'founta':4,'hatelingo':5,'offenseval_c':3,'semeval_a':11}
    final_dim=opt.NUM_HIDDEN
    
    if opt.MODEL=='cnn':
        final_dim=len(opt.FILTER_SIZE.split(',')*opt.NUM_FILTER)
    for task in datasets:
        dim=task2dim[task]
        fc=SimpleClassifier(fin_times*final_dim,opt.MID_DIM,dim,opt.FC_DROPOUT).cuda()
        fcs.append(fc)

    if mode=='cnn':
        #this is actually cnn but named as rnn to arrange the code
        shared_rnn=CNN_Model(em_times*opt.EMB_DIM,opt.FILTER_SIZE,opt.NUM_FILTER)
        private_rnn=clones(CNN_Model(em_times*opt.EMB_DIM,opt.FILTER_SIZE,opt.NUM_FILTER),num_tasks)
    else:
        shared_rnn=BiLSTMPacked(em_times*opt.EMB_DIM,opt.NUM_HIDDEN,opt.NUM_LAYER,opt.BIDIRECT,opt.L_RNN_DROPOUT)
        private_rnn=clones(BiLSTMPacked(em_times*opt.EMB_DIM,opt.NUM_HIDDEN,opt.NUM_LAYER,opt.BIDIRECT,opt.L_RNN_DROPOUT),num_tasks)
    
    shared_emb=Word_Embedding(dataset.dictionary.ntoken(),opt.EMB_DIM,opt.EMB_DROPOUT) 
    private_emb=clones(Word_Embedding(dataset.dictionary.ntoken(),opt.EMB_DIM,opt.EMB_DROPOUT),num_tasks)
    
    if mode in ['uniform', 'cnn', 'dnn', 'basic', 'angrybert','angrybert-attn', 'shared-bert']:
        gates=clones(Gate_Attention(opt.NUM_HIDDEN*fc_times,opt.NUM_HIDDEN*fc_times,opt.NUM_HIDDEN*fc_times).cuda(),num_tasks)
        atts = clones(SelfAttention(fc_times*opt.NUM_HIDDEN,fc_times*opt.NUM_HIDDEN,fc_times*opt.NUM_HIDDEN), num_tasks)   
        if mode in ['angrybert','angrybert-attn','shared-bert']:
            shared_rnn=model=BertForSequenceClassification.from_pretrained(
                'bert-base-uncased',
                num_labels=num_tasks,
                output_attentions=False,
                output_hidden_states=True
            )
        return Deep_Multi(shared_emb,private_emb,shared_rnn,private_rnn,gates,fcs,mode,opt,atts)
    elif mode=="mtl-gatedencoder":
        private_emb=Word_Embedding(dataset.dictionary.ntoken(),opt.EMB_DIM,opt.EMB_DROPOUT)
        gates=Gate_Attention(opt.NUM_HIDDEN*2,opt.NUM_HIDDEN*2,opt.NUM_HIDDEN*2).cuda()
        atts = clones(SelfAttention(fc_times*opt.NUM_HIDDEN,fc_times*opt.NUM_HIDDEN,fc_times*opt.NUM_HIDDEN), num_tasks)   
        private_rnn = BiLSTMPacked(em_times*opt.EMB_DIM,opt.NUM_HIDDEN,opt.NUM_LAYER,opt.BIDIRECT,opt.L_RNN_DROPOUT)
        rnn_last =BiLSTMPacked(2*opt.NUM_HIDDEN,opt.NUM_HIDDEN,opt.NUM_LAYER,opt.BIDIRECT,opt.L_RNN_DROPOUT)
        return Joint_Multi(shared_emb,private_emb,shared_rnn,private_rnn,gates,fcs,mode,opt,atts,rnn_last)

    elif mode=='couple':
        couple= Coupled_Layer(opt.EMB_DIM,opt.NUM_HIDDEN,num_tasks)
        return Deep_Coupled(shared_emb,private_emb,couple,fcs)
    elif mode=='local':
        couple=Local_Layer(opt.EMB_DIM,opt.NUM_HIDDEN,num_tasks)
        return Deep_Coupled(shared_emb,private_emb,couple,fcs)
    elif mode=='sp-mtl':
        gate=Gate_Attention(opt.NUM_HIDDEN*2,opt.NUM_HIDDEN,opt.NUM_HIDDEN)
        couple=Shared_Layer(opt.EMB_DIM,opt.NUM_HIDDEN,num_tasks,gate)
        return Deep_Coupled(shared_emb,private_emb,couple,fcs)