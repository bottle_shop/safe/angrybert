MODEL : joint-modeling
GLOVE_PATH : /data/data_store/rabiul/pre-embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : /student/mda219/workspace/angrybert/resource_v2
EMB_DROPOUT : 0.3
FC_DROPOUT : 0.2
MIN_OCC : 3
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 256
PROJ_DIM : 32
NUM_HIDDEN : 256
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : True
L_RNN_DROPOUT : 0.1
TASKS : founta,emotion,hatelingo,semeval_c
LENGTH : 30
CREATE_DICT : True
CREATE_EMB : True
SAVE_NUM : 1
EPOCHS : 6
CROSS_VAL : 5
SEED : 1112
CUDA_DEVICE : 2
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 270.63
	eval task founta precision: 80.92 recall: 81.96 f1: 80.74
	eval task founta hate precision: 70.00 recall: 26.00 f1: 38.00

	eval task emotion roc auc score for multi label classification: 65.87 

	eval task hatelingo precision: 71.72 recall: 55.26 f1: 58.60
	eval task semeval_c precision: 37.54 recall: 61.27 f1: 46.55
epoch 1
	 multi task train_loss: 130.08
	eval task founta precision: 80.54 recall: 81.76 f1: 80.65
	eval task founta hate precision: 64.00 recall: 29.00 f1: 40.00

	eval task emotion roc auc score for multi label classification: 76.98 

	eval task hatelingo precision: 87.55 recall: 84.91 f1: 85.22
	eval task semeval_c precision: 64.33 recall: 68.21 f1: 65.35
epoch 2
	 multi task train_loss: 82.02
	eval task founta precision: 79.50 recall: 80.76 f1: 79.84
	eval task founta hate precision: 47.00 recall: 37.00 f1: 42.00

	eval task emotion roc auc score for multi label classification: 80.34 

	eval task hatelingo precision: 89.88 recall: 71.58 f1: 77.01
	eval task semeval_c precision: 65.34 recall: 72.23 f1: 68.53
epoch 3
	 multi task train_loss: 57.48
	eval task founta precision: 79.29 recall: 80.40 f1: 79.68
	eval task founta hate precision: 50.00 recall: 34.00 f1: 40.00

	eval task emotion roc auc score for multi label classification: 81.89 

	eval task hatelingo precision: 90.24 recall: 80.96 f1: 84.21
	eval task semeval_c precision: 60.60 recall: 68.82 f1: 63.73
epoch 4
	 multi task train_loss: 43.33
	eval task founta precision: 78.75 recall: 80.11 f1: 79.14
	eval task founta hate precision: 53.00 recall: 31.00 f1: 39.00

	eval task emotion roc auc score for multi label classification: 82.35 

	eval task hatelingo precision: 90.03 recall: 84.65 f1: 86.40
	eval task semeval_c precision: 65.80 recall: 68.45 f1: 66.80
epoch 5
	 multi task train_loss: 35.65
	eval task founta precision: 78.25 recall: 79.33 f1: 78.67
	eval task founta hate precision: 46.00 recall: 35.00 f1: 40.00

	eval task emotion roc auc score for multi label classification: 82.69 

	eval task hatelingo precision: 90.55 recall: 88.60 f1: 88.98
	eval task semeval_c precision: 65.06 recall: 67.24 f1: 66.01
