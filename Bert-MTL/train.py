import os
import time
import torch
import torch.nn as nn
import utils
import torch.nn.functional as F
import config
import numpy as np
import h5py
import pickle as pkl
import json
import random
from transformers import get_linear_schedule_with_warmup,AdamW
from torch.utils.data import DataLoader
from sklearn.utils.multiclass import type_of_target
from sklearn.metrics import f1_score,recall_score,precision_score,accuracy_score,classification_report,precision_recall_fscore_support,roc_auc_score
from utils import NpEncoder, attention_dump


def log_hyperpara(logger,opt):
    dic = vars(opt)
    for k,v in dic.items():
        logger.write(k + ' : ' + str(v))

def bce_for_loss(logits,labels):
    loss=nn.functional.binary_cross_entropy_with_logits(logits, labels)
    # loss*=labels.size(1)
    #print (loss)
    return loss

def compute_auc(logits,labels):
    result=roc_auc_score(labels.cpu().numpy(),logits.cpu().numpy(),average='weighted')
    return result

def compute_score(logits,labels):
    logits=torch.max(logits,1)[1]
    labels=torch.max(labels,1)[1]
    score=logits.eq(labels)
    score=score.sum().float()
    return score

def compute_other(logits,labels,source=None):
    acc=compute_score(logits,labels)
    logits=np.argmax(logits.cpu().numpy(),axis=1)
    label=np.argmax(labels.cpu().numpy(),axis=1)
    length=logits.shape[0]

    f1=f1_score(label,logits,average='weighted',labels=np.unique(label))
    recall=recall_score(label,logits,average='weighted',labels=np.unique(label))
    precision=precision_score(label,logits,average='weighted',labels=np.unique(label))

    result=classification_report(label,logits)
    #if source in ["wz","dt","founta"]:
     #   print (result)
    information=result.split('\n')
    #print(information,result)
    cur=information[2].split('     ')
    h_p=float(cur[3].strip())
    h_r=float(cur[4].strip())
    h_f=float(cur[5].strip())
    total=[]
    
    total.append(precision*100)
    total.append(recall*100)
    total.append(f1*100)
    total.append(h_p*100)
    total.append(h_r*100)
    total.append(h_f*100)
    return total


def setup_optimizer(model,mtl_loss):
    exclude = "s_rnn"
    exclude_2 = "proj"
    bert_params = list(filter(lambda kv:kv[0] if kv[0].startswith(exclude) or kv[0].startswith(exclude_2) else None, model.named_parameters()))
    other_params = list(filter(lambda kv:kv[0] if not kv[0].startswith(exclude) and not kv[0].startswith(exclude_2) else None, model.named_parameters()))
                 
    bert_params_value= []
    other_params_value = []
    for name, value in bert_params:
        bert_params_value.append(value)
    for name, value in other_params:
        other_params_value.append(value)
    for name, value in mtl_loss.named_parameters():
        other_params_value.append(value)

    
    optimizer = torch.optim.AdamW([{"params":bert_params_value[:100], "lr":2e-5}, {"params":bert_params_value[100:], "lr":5e-5}, {"params":other_params_value}], lr=1e-3, eps=1e-9)
    return optimizer

def freeze(model):
    exclude = "s_rnn"
    exclude_2 = "proj"
    bert_params = list(filter(lambda kv:kv[0] if kv[0].startswith(exclude) or kv[0].startswith(exclude_2) else None, model.named_parameters()))
                
    for name, value in bert_params:
        value.requires_grad = False


class MultiTaskLoss(nn.Module):
    def __init__(self, tasks):
        super(MultiTaskLoss, self).__init__()
        self.tasks = tasks
        self.log_vars = nn.Parameter(torch.zeros(len(self.tasks)).cuda(), requires_grad=True)

    def forward(self, preds, targets, task_idx):
        loss = bce_for_loss(preds, targets)
        precision_0 = torch.exp(-self.log_vars[task_idx])
        loss_ = precision_0 * loss
        return loss_

def train_for_deep(opt,model,total_train,total_test,total_val):
    tasks=opt.TASKS.split(',')
    #total_iters=[total_train[i].num_iters for i in range(len(tasks))]
    total_iters=[total_train[i].num_iters for i in range(len(tasks))]
    max_iters=sum(total_iters)
    if tasks[0]=='dt':
        logger=utils.Logger(os.path.join(opt.DT,'log'+str(opt.SAVE_NUM)+'.txt'))
    elif tasks[0]=='wz':
        logger=utils.Logger(os.path.join(opt.WZ_RESULT,'log'+str(opt.SAVE_NUM)+'.txt'))
    elif tasks[0]=='founta':
        logger=utils.Logger(os.path.join(opt.FOUNTA_RESULT,'log'+str(opt.SAVE_NUM)+'.txt'))
    log_hyperpara(logger,opt)

    mtl_loss=MultiTaskLoss(tasks)
    """
    all tasks have the same number of training epochs
    now for evaluating on other tasks, I just wanna to 

    WARNING!!!! swith optimizer to setup_optimizer() for angrybert
    """
    #total_loss=0.0
    
    if opt.MODEL in ['angrybert','angrybert-attn','shared-bert']:
        optimizer = setup_optimizer(model, mtl_loss)
    else:
        optimizer= torch.optim.AdamW(list(model.parameters())+list(mtl_loss.parameters()),lr=1e-3,eps=1e-8)

    scheduler=get_linear_schedule_with_warmup(optimizer,
                                              num_warmup_steps=0,
                                              num_training_steps=max_iters*opt.EPOCHS
                                             )
    best_on_epoch = None
    f1_max = 0
    for epoch in range(opt.EPOCHS):
        total_loss = [0.0 for _ in range(len(tasks))]
        cur_iters = [0 for _ in range(len(tasks))]
        # task to task index
        cur_tasks = {task: i for i, task in enumerate(tasks)}
        for iters in range(max_iters):
            """
            choose a task among the remaining tasks
            """
            choices=[cur_tasks[name] for name in cur_tasks.keys()]
            task_idx=random.choice(choices)
            batch_info=total_train[task_idx].next_batch()
            tokens=batch_info['tokens'].cuda()
            bert_tokens=batch_info['bert_tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            masks=batch_info['masks'].cuda()
            att_masks=batch_info['att_masks'].cuda()
            pred,_=model(tokens,task_idx,bert_tokens,masks,att_masks)
            loss=mtl_loss(pred,labels,task_idx)
            total_loss[task_idx]+=loss
            loss.backward()
            nn.utils.clip_grad_norm_(model.parameters(),1.0)
            optimizer.step()
            scheduler.step()#updating the learning rate
            optimizer.zero_grad()

            cur_iters[task_idx]+=1
            upper_bound = total_iters[task_idx]
            if cur_iters[task_idx]>=upper_bound:
                cur_tasks.pop(tasks[task_idx])

        logger.write('epoch %d' %(epoch))
        ttl_loss=np.sum(total_loss)
        logger.write('\t multi task train_loss: %.2f' % (ttl_loss))
        #re-initialize the records for loss
        total=[]
        print ('Evaluating on each task')
        #for basic MTL, roc is the F1 for the lingo task
        model.train(False)
        for j,task in enumerate(tasks):
            print(j, task)
            cur_total=evaluate_for_offensive(opt,model,total_test[j],j)
            total.append(cur_total)
            if j == 0:
                eval_score=evaluate_for_offensive(opt,model,total_val,j)
                f1_cur = eval_score[2]
                
                if f1_cur >= f1_max:
                    f1_max = f1_cur
                    best_on_epoch = cur_total

                    print('\tVALIDATION task %s precision: %.2f recall: %.2f f1: %.2f' % (task, eval_score[0], eval_score[1], eval_score[2]))
            if task in ['dt','founta']:
                logger.write('\teval task %s precision: %.2f recall: %.2f f1: %.2f' % (task, cur_total[0], cur_total[1], cur_total[2]))
                logger.write('\teval task %s hate precision: %.2f recall: %.2f f1: %.2f\n' % (task, cur_total[3],cur_total[4],cur_total[5]))
            elif task == 'semeval_a':
                logger.write('\teval task %s roc auc score for multi label classification: %.2f \n' % (task,  cur_total[0]))
            else:
                logger.write('\teval task %s precision: %.2f recall: %.2f f1: %.2f' % (task, cur_total[0], cur_total[1], cur_total[2]))
        model.train(True)
    total[0] = best_on_epoch
    return total

def evaluate_for_offensive(opt,model,test_set,task_idx):
    # print ('The information for task 1 iterations is:',len(test_set),test_set.last_batch)
    task=opt.TASKS.split(',')[task_idx]
    total = len(test_set)
    for i in range(total):
        with torch.no_grad():
            batch_info=test_set.next_batch()
            tokens=batch_info['tokens'].cuda()
            bert_tokens=batch_info['bert_tokens'].cuda()
            labels=batch_info['label'].float().cuda()
            masks=batch_info['masks'].cuda()
            att_masks=batch_info['att_masks'].cuda()
            pred,_=model(tokens,task_idx,bert_tokens,masks,att_masks)

        if i==0:
            t1_labels=labels
            t1_pred=pred
        else:
            t1_labels=torch.cat((t1_labels,labels),0)
            t1_pred=torch.cat((t1_pred,pred),0)
    total=compute_other(t1_pred,t1_labels,task)
    if task == "semeval_a":
        total[0]=compute_auc(t1_pred,t1_labels)*100
    return total


def analysis_dump(opt,model,test_set,idx):
    tasks = opt.TASKS.split(',')
    total = len(test_set)

    result = dict()
    for task in tasks:
        result[task] = {"weights":[], "preds":[]}
        result["words"] = []
        result["labels"] = []
    for i in range(total):
        with torch.no_grad():
            batch_info = test_set.next_batch()
            tokens = batch_info['tokens'].cuda()
            labels = batch_info['label'].float().cuda()
            bert_tokens = batch_info['bert_tokens'].cuda()
            masks = batch_info['masks'].cuda()
            att_masks = batch_info['att_masks'].cuda()
            words = batch_info['words']
            result["words"].extend(words)
            result["labels"].extend(labels.cpu().numpy())
            for task_idx, task in enumerate(tasks):
                pred, _ = model(tokens, task_idx, bert_tokens, masks, att_masks)
                pred = pred.cpu().numpy()
                result[task]["preds"].extend(pred)

    write_json(result, tasks[0],idx)

def write_json(result, source, idx):
    with open("weights/{}.mtddn.analysis.json".format(source+str(idx)), "w") as f:
        json.dump(result, f, cls=NpEncoder)
