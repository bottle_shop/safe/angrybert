validation folder 1
	eval task dt precision: 89.61 
	eval task dt recall: 88.96 
	eval task dt f1: 89.25 
	eval task dt hate precision: 39.00 
	eval task dt hate recall: 47.00 
	eval task dt hate f1: 43.00 

	eval task hatelingo precision: 93.21 
	eval task hatelingo recall: 92.33 
	eval task hatelingo f1: 90.84 

	eval task semeval_c precision: 71.94 
	eval task semeval_c recall: 75.76 
	eval task semeval_c f1: 72.41 

	eval task emotion roc auc score for multi label classification: 86.94 

validation folder 2
	eval task dt precision: 89.04 
	eval task dt recall: 90.36 
	eval task dt f1: 89.50 
	eval task dt hate precision: 43.00 
	eval task dt hate recall: 23.00 
	eval task dt hate f1: 30.00 

	eval task hatelingo precision: 96.80 
	eval task hatelingo recall: 96.65 
	eval task hatelingo f1: 96.50 

	eval task semeval_c precision: 69.63 
	eval task semeval_c recall: 73.56 
	eval task semeval_c f1: 69.99 

	eval task emotion roc auc score for multi label classification: 86.66 

validation folder 3
	eval task dt precision: 90.18 
	eval task dt recall: 91.51 
	eval task dt f1: 90.45 
	eval task dt hate precision: 53.00 
	eval task dt hate recall: 23.00 
	eval task dt hate f1: 32.00 

	eval task hatelingo precision: 98.98 
	eval task hatelingo recall: 98.94 
	eval task hatelingo f1: 98.95 

	eval task semeval_c precision: 64.63 
	eval task semeval_c recall: 72.71 
	eval task semeval_c f1: 68.31 

	eval task emotion roc auc score for multi label classification: 85.93 

validation folder 4
	eval task dt precision: 90.32 
	eval task dt recall: 91.71 
	eval task dt f1: 90.47 
	eval task dt hate precision: 56.00 
	eval task dt hate recall: 19.00 
	eval task dt hate f1: 29.00 

	eval task hatelingo precision: 99.13 
	eval task hatelingo recall: 99.12 
	eval task hatelingo f1: 99.12 

	eval task semeval_c precision: 65.57 
	eval task semeval_c recall: 72.46 
	eval task semeval_c f1: 68.76 

	eval task emotion roc auc score for multi label classification: 87.87 

validation folder 5
	eval task dt precision: 89.60 
	eval task dt recall: 91.17 
	eval task dt f1: 89.89 
	eval task dt hate precision: 50.00 
	eval task dt hate recall: 18.00 
	eval task dt hate f1: 26.00 

	eval task hatelingo precision: 90.11 
	eval task hatelingo recall: 85.44 
	eval task hatelingo f1: 86.82 

	eval task semeval_c precision: 67.51 
	eval task semeval_c recall: 70.04 
	eval task semeval_c f1: 67.43 

	eval task emotion roc auc score for multi label classification: 87.41 


 final result
	eval task dt precision: 89.75 
	eval task dt recall: 90.74 
	eval task dt f1: 89.91 
	eval task dt hate precision: 48.20 
	eval task dt hate recall: 26.00 
	eval task dt hate f1: 32.00 

	eval task hatelingo precision: 95.64 
	eval task hatelingo recall: 94.50 
	eval task hatelingo f1: 94.44 
	eval task semeval_c precision: 67.86 
	eval task semeval_c recall: 72.91 
	eval task semeval_c f1: 69.38 
	eval task emotion roc auc score for multi label classification: 86.96 

