MODEL : fusion
GLOVE_PATH : /home/ruicao/trained/embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : ../angry-MTL/split_data_tokens
EMB_DROPOUT : 0.5
FC_DROPOUT : 0.2
MIN_OCC : 5
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 128
PROJ_DIM : 32
NUM_HIDDEN : 200
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : False
L_RNN_DROPOUT : 0.3
TASKS : dt,harassment
LENGTH : 30
CREATE_DICT : True
CREATE_EMB : True
SAVE_NUM : 5
EPOCHS : 6
CROSS_VAL : 10
SEED : 1111
CUDA_DEVICE : 7
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 1.38
	eval task dt precision: 64.64 
	eval task dt recall: 20.63 
	eval task dt f1: 15.99 
	eval task dt hate precision: 2.00 
	eval task dt hate recall: 2.00 
	eval task dt hate f1: 2.00 

	eval task harassment precision: 60.22 
	eval task harassment recall: 73.24 
	eval task harassment f1: 62.90 

epoch 1
	 multi task train_loss: 190.16
	eval task dt precision: 84.12 
	eval task dt recall: 89.02 
	eval task dt f1: 86.46 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 57.07 
	eval task harassment recall: 71.44 
	eval task harassment f1: 61.62 

epoch 2
	 multi task train_loss: 162.70
	eval task dt precision: 85.02 
	eval task dt recall: 90.30 
	eval task dt f1: 87.58 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 56.31 
	eval task harassment recall: 67.63 
	eval task harassment f1: 60.79 

epoch 3
	 multi task train_loss: 156.40
	eval task dt precision: 85.20 
	eval task dt recall: 90.54 
	eval task dt f1: 87.78 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 60.42 
	eval task harassment recall: 72.36 
	eval task harassment f1: 63.13 

epoch 4
	 multi task train_loss: 152.61
	eval task dt precision: 86.20 
	eval task dt recall: 91.43 
	eval task dt f1: 88.71 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 58.33 
	eval task harassment recall: 71.09 
	eval task harassment f1: 62.21 

epoch 5
	 multi task train_loss: 150.23
	eval task dt precision: 83.57 
	eval task dt recall: 88.90 
	eval task dt f1: 85.96 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 59.75 
	eval task harassment recall: 65.82 
	eval task harassment f1: 62.04 

epoch 6
	 multi task train_loss: 146.08
	eval task dt precision: 85.34 
	eval task dt recall: 90.62 
	eval task dt f1: 87.90 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 61.15 
	eval task harassment recall: 69.19 
	eval task harassment f1: 63.61 

epoch 7
	 multi task train_loss: 142.99
	eval task dt precision: 85.37 
	eval task dt recall: 90.67 
	eval task dt f1: 87.93 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 59.98 
	eval task harassment recall: 67.48 
	eval task harassment f1: 62.61 

epoch 8
	 multi task train_loss: 140.77
	eval task dt precision: 84.95 
	eval task dt recall: 90.30 
	eval task dt f1: 87.54 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task harassment precision: 61.30 
	eval task harassment recall: 67.14 
	eval task harassment f1: 63.44 

epoch 9
	 multi task train_loss: 137.34
	eval task dt precision: 88.52 
	eval task dt recall: 90.87 
	eval task dt f1: 88.19 
	eval task dt hate precision: 50.00 
	eval task dt hate recall: 1.00 
	eval task dt hate f1: 1.00 

	eval task harassment precision: 62.43 
	eval task harassment recall: 71.34 
	eval task harassment f1: 64.11 

