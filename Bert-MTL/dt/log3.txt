MODEL : bert
GLOVE_PATH : /home/ruicao/trained/embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : ../angry-MTL/split_data_tokens
EMB_DROPOUT : 0.5
FC_DROPOUT : 0.2
MIN_OCC : 3
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 128
PROJ_DIM : 32
NUM_HIDDEN : 200
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : False
L_RNN_DROPOUT : 0.3
TASKS : dt,semeval_a
LENGTH : 30
CREATE_DICT : True
CREATE_EMB : True
SAVE_NUM : 3
EPOCHS : 6
CROSS_VAL : 5
SEED : 1111
CUDA_DEVICE : 6
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 1.37
	eval task dt precision: 65.30 
	eval task dt recall: 70.99 
	eval task dt f1: 67.97 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 60.63 
	eval task semeval_a recall: 66.67 
	eval task semeval_a f1: 57.24 

epoch 1
	 multi task train_loss: 0.85
	eval task dt precision: 83.36 
	eval task dt recall: 88.60 
	eval task dt f1: 85.78 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 79.87 
	eval task semeval_a recall: 80.14 
	eval task semeval_a f1: 79.97 

epoch 2
	 multi task train_loss: 0.49
	eval task dt precision: 84.71 
	eval task dt recall: 89.98 
	eval task dt f1: 87.25 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 80.45 
	eval task semeval_a recall: 80.73 
	eval task semeval_a f1: 80.54 

epoch 3
	 multi task train_loss: 0.63
	eval task dt precision: 84.69 
	eval task dt recall: 89.96 
	eval task dt f1: 87.25 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 80.17 
	eval task semeval_a recall: 80.62 
	eval task semeval_a f1: 80.15 

epoch 4
	 multi task train_loss: 0.59
	eval task dt precision: 84.90 
	eval task dt recall: 90.12 
	eval task dt f1: 87.41 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 80.11 
	eval task semeval_a recall: 80.14 
	eval task semeval_a f1: 80.12 

epoch 5
	 multi task train_loss: 0.42
	eval task dt precision: 84.99 
	eval task dt recall: 90.22 
	eval task dt f1: 87.51 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 79.84 
	eval task semeval_a recall: 80.10 
	eval task semeval_a f1: 79.94 

epoch 6
	 multi task train_loss: 0.32
	eval task dt precision: 88.65 
	eval task dt recall: 90.00 
	eval task dt f1: 87.39 
	eval task dt hate precision: 67.00 
	eval task dt hate recall: 1.00 
	eval task dt hate f1: 3.00 

	eval task semeval_a precision: 80.12 
	eval task semeval_a recall: 79.83 
	eval task semeval_a f1: 79.95 

epoch 7
	 multi task train_loss: 0.31
	eval task dt precision: 85.15 
	eval task dt recall: 90.38 
	eval task dt f1: 87.68 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task semeval_a precision: 79.70 
	eval task semeval_a recall: 80.17 
	eval task semeval_a f1: 79.70 

epoch 8
	 multi task train_loss: 0.39
	eval task dt precision: 89.42 
	eval task dt recall: 90.32 
	eval task dt f1: 87.86 
	eval task dt hate precision: 75.00 
	eval task dt hate recall: 3.00 
	eval task dt hate f1: 6.00 

	eval task semeval_a precision: 79.50 
	eval task semeval_a recall: 79.86 
	eval task semeval_a f1: 79.60 

epoch 9
	 multi task train_loss: 0.31
	eval task dt precision: 87.98 
	eval task dt recall: 90.32 
	eval task dt f1: 87.97 
	eval task dt hate precision: 48.00 
	eval task dt hate recall: 3.00 
	eval task dt hate f1: 6.00 

	eval task semeval_a precision: 79.60 
	eval task semeval_a recall: 79.93 
	eval task semeval_a f1: 79.70 

