MODEL : fusion
GLOVE_PATH : /home/ruicao/trained/embeddings/glove.6B.300d.txt
DT : ./dt
WZ_RESULT : ./wz
FOUNTA_RESULT : ./founta
HATELINGO_RESULT : ./hatelingo
DICT_INFO : ./dictionary
SPLIT_DATASET : ../angry-MTL/split_data_tokens
EMB_DROPOUT : 0.5
FC_DROPOUT : 0.2
MIN_OCC : 5
BATCH_SIZE : 64
EMB_DIM : 300
MID_DIM : 128
PROJ_DIM : 32
NUM_HIDDEN : 200
NUM_FILTER : 150
FILTER_SIZE : 2,3,4
NUM_LAYER : 1
BIDIRECT : False
L_RNN_DROPOUT : 0.3
TASKS : dt,hatelingo
LENGTH : 30
CREATE_DICT : True
CREATE_EMB : True
SAVE_NUM : 4
EPOCHS : 6
CROSS_VAL : 10
SEED : 1111
CUDA_DEVICE : 7
EVAL_ITERS : 180
TOTAL_ITERS : 1800
epoch 0
	 multi task train_loss: 1.38
	eval task dt precision: 63.80 
	eval task dt recall: 67.39 
	eval task dt f1: 65.40 
	eval task dt hate precision: 8.00 
	eval task dt hate recall: 1.00 
	eval task dt hate f1: 2.00 

	eval task hatelingo precision: 3.99 
	eval task hatelingo recall: 5.99 
	eval task hatelingo f1: 1.67 

epoch 1
	 multi task train_loss: 184.03
	eval task dt precision: 84.06 
	eval task dt recall: 89.26 
	eval task dt f1: 86.59 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 62.31 
	eval task hatelingo recall: 77.00 
	eval task hatelingo f1: 68.72 

epoch 2
	 multi task train_loss: 143.11
	eval task dt precision: 85.65 
	eval task dt recall: 90.67 
	eval task dt f1: 88.01 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 80.95 
	eval task hatelingo recall: 81.42 
	eval task hatelingo f1: 76.57 

epoch 3
	 multi task train_loss: 135.47
	eval task dt precision: 85.08 
	eval task dt recall: 90.54 
	eval task dt f1: 87.72 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 87.24 
	eval task hatelingo recall: 86.55 
	eval task hatelingo f1: 84.23 

epoch 4
	 multi task train_loss: 131.93
	eval task dt precision: 84.97 
	eval task dt recall: 90.30 
	eval task dt f1: 87.51 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 89.07 
	eval task hatelingo recall: 88.89 
	eval task hatelingo f1: 87.50 

epoch 5
	 multi task train_loss: 130.26
	eval task dt precision: 86.15 
	eval task dt recall: 91.59 
	eval task dt f1: 88.78 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 88.86 
	eval task hatelingo recall: 86.11 
	eval task hatelingo f1: 84.69 

epoch 6
	 multi task train_loss: 128.08
	eval task dt precision: 84.94 
	eval task dt recall: 90.30 
	eval task dt f1: 87.50 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 89.06 
	eval task hatelingo recall: 86.11 
	eval task hatelingo f1: 84.58 

epoch 7
	 multi task train_loss: 126.69
	eval task dt precision: 86.16 
	eval task dt recall: 91.47 
	eval task dt f1: 88.74 
	eval task dt hate precision: 0.00 
	eval task dt hate recall: 0.00 
	eval task dt hate f1: 0.00 

	eval task hatelingo precision: 89.03 
	eval task hatelingo recall: 85.85 
	eval task hatelingo f1: 84.58 

epoch 8
	 multi task train_loss: 125.39
	eval task dt precision: 89.08 
	eval task dt recall: 91.47 
	eval task dt f1: 88.84 
	eval task dt hate precision: 50.00 
	eval task dt hate recall: 1.00 
	eval task dt hate f1: 3.00 

	eval task hatelingo precision: 89.76 
	eval task hatelingo recall: 86.20 
	eval task hatelingo f1: 85.25 

epoch 9
	 multi task train_loss: 124.99
	eval task dt precision: 89.95 
	eval task dt recall: 91.51 
	eval task dt f1: 89.99 
	eval task dt hate precision: 55.00 
	eval task dt hate recall: 15.00 
	eval task dt hate f1: 23.00 

	eval task hatelingo precision: 88.64 
	eval task hatelingo recall: 87.07 
	eval task hatelingo f1: 85.51 

