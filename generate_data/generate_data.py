"""
generating datasets in five-folder for WZ,
Semeval, Gab Hate, Harassment, Toxic Detection,
and one for emotion 
"""
import argparse 
import pickle as pkl
import pandas as pd
import random
import os
import numpy as np
import csv
from transformers import BertTokenizer
from preprocessing import clean_text
if __name__ == '__main__':
    MAX_LEN=64
    parser=argparse.ArgumentParser()
    parser.add_argument('--DATASET',type=str,default='wz')
    parser.add_argument('--NUM_FOLDER',type=int,default=5)
    args=parser.parse_args()
    tokenizer=BertTokenizer.from_pretrained('bert-base-uncased')
    
    PATH='/home/ruicao/NLP/datasets/hate-speech'
    SAVE_PATH='/home/ruicao/NLP/datasets/hate-speech-tokens'
    #np.set_printoptions(suppress=True,threshold=17)
    
    """
    Three tasks:
    offensive or not
    if offensive, target or not
    if target, three kinds of targets
    """
    if args.DATASET=='emotion':
        """
        please refer to https://competitions.codalab.org/competitions/17751#learn_the_details-datasets for more details
        here just use the classification task for emotions, test file does not contain the answers
        total posts: 6839 posts with no emotions: 205
        0-anger-2544
        1-anticipation-978
        2-disgust-2602
        3-fear-1242
        4-joy-2477
        5-love-700
        6-optimism-1984
        7-pessimism-795
        8-sadness-2008
        9-surprise-361
        10-trust-357
        """
        entries=[]
        count=0
        count_dict={i:0 for i in range(11)}
        train=open(os.path.join(PATH,'emotion/2018-E-c-En-train.txt'),'r').readlines()
        #test=open('./2018-E-c-En-test.txt','r').readlines()
        for row in train:
            info=row.strip().split('\t')
            sent=info[1]
            '''sent=tokenizer.encode(
                sent,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            answer=[0 for i in range(11)]
            labels=info[2:]
            for i,l in enumerate(labels):
                if l=='1':
                    answer[i]=1.0
                    count_dict[i]+=1
            if sum(answer)==0:
                count+=1
            entry={
                'sent':sent,
                'label':np.array(answer)
            }
            entries.append(entry)
        random.shuffle(entries)
        print (len(entries),count,count_dict)
        num_folder=int(len(entries)/args.NUM_FOLDER)
        total={}
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_entries=entries[i*num_folder:]
            else:
                cur_entries=entries[i*num_folder:(i+1)*num_folder]
            print ('Folder:',i,len(cur_entries))
            total[str(i)]=cur_entries
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='gab':
        """
        use it as a target classification dataset
        86529 posts in total and 11249 can be used as posts for target classification
        1627 posts do not contain any targets above
        0-REL-2038 
        1-RAE-2611 
        2-SXO-558 
        3-GEN-808 
        4-IDL-1610 
        5-NAT-1575 
        6-POL-2773 
        7-PH-314
        """
        anno=pd.read_csv(os.path.join(PATH,'gab/GabHateCorpus_annotations.tsv'),sep='\t')
        labels=anno[['REL','RAE','SXO','GEN','IDL','NAT','POL','MPH']]
        tweets=anno['Text']
        entries=[]
        count=0
        count_dict={i:0 for i in range(8)}
        for i in range(len(labels)):
            result=labels.loc[i]
            if np.isnan(result['REL']):
                continue
            answer=[0,0,0,0,0,0,0,0]
            '''sent=tokenizer.encode(
                tweets[i],
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            sent=tweets[i]
            for j,name in enumerate(result.keys()):
                answer[j]=result[name]
                if result[name]>0:
                    count_dict[j]+=1
            if sum(answer)==0:
                count+=1
            entry={
                'sent':sent,
                'label':np.array(answer)
            }
            entries.append(entry)
        random.shuffle(entries)
        print (count,count_dict)
        #print (entries[:10])
        num_folder=int(len(entries)/args.NUM_FOLDER)
        total={}
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_entries=entries[i*num_folder:]
            else:
                cur_entries=entries[i*num_folder:(i+1)*num_folder]
            print ('Folder:',i,len(cur_entries))
            total[str(i)]=cur_entries
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='toxic':
        train_df=pd.read_csv(os.path.join(PATH,'toxic-comment-detection/train.csv'))
        test=pd.read_csv(os.path.join(PATH,'toxic-comment-detection/test.csv'))
        labels=pd.read_csv(os.path.join(PATH,'toxic-comment-detection/test_labels.csv'))
        test_df=pd.concat([test['comment_text'],labels],axis=1)
        label_names=['toxic', 'severe_toxic', 'obscene', 'threat','insult', 'identity_hate']
        entries=[]
        
        texts=train_df['comment_text']
        truth=train_df[label_names].values
        for i,row in enumerate(texts):
            sent=row
            '''sent=tokenizer.encode(
                row,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            entry={
                'sent':sent,
                'label':truth[i]
            }
            entries.append(entry)
        print(len(entries))
        texts=test_df['comment_text']
        truth=test_df[label_names].values
        for i,row in enumerate(texts):
            if truth[i][0]==-1:
                continue
            '''sent=tokenizer.encode(
                row,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            sent=row
            entry={
                'sent':sent,
                'label':truth[i]
            }
            entries.append(entry)
        print(len(entries))
        random.shuffle(entries)
        
        num_folder=int(len(entries)/args.NUM_FOLDER)
        total={}
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_entries=entries[i*num_folder:]
            else:
                cur_entries=entries[i*num_folder:(i+1)*num_folder]
            print ('Folder:',i,len(cur_entries))
            total[str(i)]=cur_entries
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='wz':
        """
        0-racism-1923
        1-sexism-3079
        2-neither-11033
        """
        labels=open(os.path.join(PATH,'wz/NAACL_SRW_2016.csv'),'r',encoding='ISO-8859-1').readlines()
        #constructing the relation between ids and labels
        ans_dict={}
        for row in labels:
            info=row.split(',')
            ans=info[1].strip()
            label=2
            if ans=='racism':
                label=0
            elif ans=='sexism':
                label=1
            ans_dict[str(info[0])]=label
        sexism=[]
        racism=[]
        normal=[]
        total={}    
        with open(os.path.join(PATH,'wz/zeerak_waseem_2016.csv'),'r',encoding='unicode_escape') as f:
            reader=csv.DictReader(f)
            for index, line in enumerate(reader):
                sent=line['Tweets']
                if sent is None:
                    continue
                '''sent=clean_text(sent)
                sent=tokenizer.encode(
                    sent,
                    add_special_tokens=True, # add [CLS] and [SEP]
                    max_length=MAX_LEN,
                    truncation=True
                )'''
                idx=line['ID']
                if idx==None:
                    continue
                    print (index)
                ans=ans_dict[idx]
                if ans==0:
                    racism.append(sent)
                elif ans==1:
                    sexism.append(sent)
                else:
                    normal.append(sent)
            print (index)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex))
            entries=[]
            for sent in cur_rac:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_sex:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':2
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='hatelingo':
        total={}
        dataset=pkl.load(open(os.path.join(PATH,'hatelingo/hatelingo_raw.pkl'),'rb'))
        for i in range(5):
            info=dataset[str(i)]
            cur_dict={j:[] for j in range(5)}
            for row in info:
                sent=row['sent']
                '''sent=tokenizer.encode(
                    sent,
                    add_special_tokens=True, # add [CLS] and [SEP]
                    max_length=MAX_LEN,
                    truncation=True
                )'''
                ans=row['label']
                cur_dict[ans].append(sent)
            sp_first=[]
            sp_second=[]
            for name in cur_dict.keys():
                split=int(len(cur_dict[name]) / 2)
                first=cur_dict[name][:split]
                second=cur_dict[name][split:]
                #print (name,split)
                for sent in first:
                    entry={
                        'sent':sent,
                        'label':name
                    }
                    sp_first.append(entry)
                print ('First split',len(first),'Class',name)
                for sent in second:
                    entry={
                        'sent':sent,
                        'label':name
                    }
                    sp_second.append(entry)
            print ('First split',len(sp_second))
            total[str(i)]=sp_first
            total[str(i+5)]=sp_second
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='founta':
        """
        0-hate-4965
        1-abusive-27150
        2-normal-53851
        3-spam-14030
        """
        labels=pd.read_csv(os.path.join(PATH,'founta/hatespeech_text_label_vote_RESTRICTED.csv'),sep='\t',header=None)
        sexism=[]
        racism=[]
        normal=[]
        spam=[]
        total={}    
        for d in labels.iterrows():
            t=d[1][0]
            '''t=clean_text(t)
            t=tokenizer.encode(
                t,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            ans=d[1][1].strip()
            if ans=='hateful':
                racism.append(t)
            elif ans=='abusive':
                sexism.append(t)
            elif ans=='normal':
                normal.append(t)
            elif ans=='spam':
                spam.append(t)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism),'Spam:',len(spam))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        num_spam=int(len(spam)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_spam=spam[i*num_spam:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_spam=spam[i*num_spam:(i+1)*num_spam]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex),'Spam:',len(cur_spam))
            entries=[]
            for sent in cur_rac:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_sex:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':2
                }
                entries.append(entry)
            for sent in cur_spam:
                entry={
                    'sent':sent,
                    'label':3
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='dt':
        """
        0-hate-1430
        1-offensive-19190
        2-neither-4163
        """
        labels=pd.read_csv(os.path.join(PATH,'dt/labeled_data.csv'))
        tweets=labels['tweet']
        classes=labels['class']
        racism=[]#hate
        sexism=[]#offensive
        normal=[]#neither
        total={}    
        for i,c in enumerate(classes):
            t=tweets[i]
            '''t=clean_text(t)
            t=tokenizer.encode(
                t,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            if c==0:
                racism.append(t)
            elif c==1:
                sexism.append(t)
            elif c==2:
                normal.append(t)
        print ('Normal:',len(normal),'Racism:',len(racism),'Sexism:',len(sexism))
        num_rac=int(len(racism)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        num_sex=int(len(sexism)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_rac=racism[i*num_rac:]
                cur_sex=sexism[i*num_sex:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_rac=racism[i*num_rac:(i+1)*num_rac]
                cur_sex=sexism[i*num_sex:(i+1)*num_sex]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Racism:',len(cur_rac),'Sexism:',len(cur_sex))
            entries=[]
            for sent in cur_rac:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_sex:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':2
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='harassment':
        """
        0-harassment-5285
        1-not_harassment-15075
        """
        harassment=pd.read_csv(os.path.join(PATH,'Harassment/onlineHarassmentDataset.tdf'),encoding='ISO-8859-1',sep='\t')
        tweets=harassment['Tweet']
        labels=harassment['Code']
        inapp=[]
        normal=[]
        total={}
        for i, sent in enumerate(tweets):
            ans=labels[i].strip()
            '''sent=tokenizer.encode(
                sent,
                add_special_tokens=True, # add [CLS] and [SEP]
                max_length=MAX_LEN,
                truncation=True
            )'''
            if ans=='H':
                inapp.append(sent)
            elif ans=='N':
                normal.append(sent)
        print ('Normal:',len(normal),'Inapproporiate:',len(inapp))
        num_inapp=int(len(inapp)/args.NUM_FOLDER)
        num_norm=int(len(normal)/args.NUM_FOLDER)
        for i in range(args.NUM_FOLDER):
            if i==args.NUM_FOLDER-1:
                cur_inapp=inapp[i*num_inapp:]
                cur_normal=normal[i*num_norm:]
            else:
                cur_inapp=inapp[i*num_inapp:(i+1)*num_inapp]
                cur_normal=normal[i*num_norm:(i+1)*num_norm]
            print ('Normal:',len(cur_normal),'Inapp:',len(cur_inapp))
            entries=[]
            for sent in cur_inapp:
                entry={
                    'sent':sent,
                    'label':0
                }
                entries.append(entry)
            for sent in cur_normal:
                entry={
                    'sent':sent,
                    'label':1
                }
                entries.append(entry)
            total[str(i)]=entries
            print (len(entries))
        pkl.dump(total,open('./data/'+args.DATASET+'.pkl','wb'))
    elif args.DATASET=='semeval':
        """
        Task A: 0-NOT-9460, 1-OFF-4640
        Task B: 0-TIN-4089, 1-UNT-551
        Task C: 0-OTH-430, 1-GRP-1152, 2-IND-2507
        """
        train_file=pd.read_csv(os.path.join(PATH,'SemEval/olid-training-v1.0.tsv'),sep='\t')
        total={'a':{'OFF':[],'NOT':[]},'b':{'TIN':[],'UNT':[]},'c':{'OTH':[],'GRP':[],'IND':[]}}
        tasks=['a','b','c']
        for row in train_file.iterrows():
            a=row[1]['subtask_a'].strip()
            b=row[1]['subtask_b']
            c=row[1]['subtask_c']
            tweet=row[1]['tweet']
            total['a'][a].append(tweet)
            if a=='OFF':
                total['b'][b.strip()].append(tweet)
                if b=='TIN':
                    total['c'][c.strip()].append(tweet)
        for task in tasks:
            labels=open(os.path.join(PATH,'SemEval/labels-level' +task+ '.csv'),'r').readlines()
            tweets=pd.read_csv(os.path.join(PATH,'SemEval/testset-level' +task+ '.tsv'),sep='\t')['tweet']
            for i,row in enumerate(labels):
                tweet=tweets[i]
                label=row.split(',')[1].strip()
                total[task][label].append(tweet)
        for l in total.keys():
            for sub in total[l].keys():
                print ('Task ',l,'label:',sub,len(total[l][sub]))
            print ('\n')    
        for task in tasks:
            info={}
            cur=total[task]
            for i in range(args.NUM_FOLDER):
                entries=[]
                for j,name in enumerate(cur.keys()):
                    num_entries=int(len(cur[name])/args.NUM_FOLDER)
                    print (j,name,num_entries)
                    if i==args.NUM_FOLDER-1:
                        sents=cur[name][i*num_entries:]
                    else:
                        sents=cur[name][i*num_entries:(i+1)*num_entries]
                    for s in sents:
                        entry={
                            'sent':s,
                            'label':j
                        }
                        entries.append(entry)
                info[str(i)]=entries
                print (task,len(info[str(i)]))
            pkl.dump(info,open('./data/semeval_'+task+'.pkl','wb'))